using Dapper.Contrib;
using Dapper.Contrib.Extensions;
namespace data.entities
{
    [Table ("Aliment")]
    public class Aliment
    {
        [Key]
        public long Id 
        {
            get;
            set;
        }

        public string NomAliment
        {
            get;
            set;
        }

        public long? IdMarque 
        {
            get;
            set;
        }
    }
}