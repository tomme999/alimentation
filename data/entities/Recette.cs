using Dapper.Contrib;
using Dapper.Contrib.Extensions;

namespace data.entities
{
    [Table ("Recette")]
    public class Recette
    {
        [Key]
        public long Id 
        {
            get;
            set;
        }

        public string nomRecette
        {
            get;
            set;
        }  
    }
}