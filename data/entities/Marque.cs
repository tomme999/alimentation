using Dapper.Contrib;
using Dapper.Contrib.Extensions;

namespace data.entities
{
    [Table ("Marque")]
    public class Marque
    {
        [Key]
        public long Id 
        {
            get;
            set;
        }

        public string NomMarque
        {
            get;
            set;
        }       
    }
}