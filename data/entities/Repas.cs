using Dapper.Contrib;
using Dapper.Contrib.Extensions;

namespace data.entities
{
    [Table ("Repas")]
    public class Repas
    {
        [Key]
        public long Id 
        {
            get;
            set;
        }

        public string nomRepas
        {
            get;
            set;
        }        
    }
}