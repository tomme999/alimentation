using Dapper.Contrib;
using Dapper.Contrib.Extensions;
namespace data.entities
{
    [Table ("Composition")]
    public class Composition
    {
         [Key]
        public long Id 
        {
            get;
            set;
        }

        public long IdAliment
        {
            get;
            set;
        }

        public long IdComposant 
        {
            get;
            set;
        }

        public decimal? Valeur
        {
            get;
            set;
        }

        public long? IdMarque 
        {
            get;
            set;
        }       
    }
}