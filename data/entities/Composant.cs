using Dapper.Contrib;
using Dapper.Contrib.Extensions;

namespace data.entities
{
    [Table ("Composant")]
    public class Composant
    {
        [Key]
        public long Id 
        {
            get;
            set;
        }

        public string NomComposant
        {
            get;
            set;
        }

        public string Unite 
        {
            get;
            set;
        }        
    }
}