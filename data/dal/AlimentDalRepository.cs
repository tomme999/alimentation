using data.entities;
using Dapper;
using Dapper.Contrib.Extensions;
using MySql;

namespace data.dal
{
    public class AlimentDalRepository:data.core.GenericDalRepository<Aliment>
    {
        public AlimentDalRepository(MySql.Data.MySqlClient.MySqlConnection connection)
        {
            Connection = connection;
        }
    }
}