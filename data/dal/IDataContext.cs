using data.entities;

namespace data.dal
{
    public interface IDataContext
    {
        AlimentDalRepository                         Aliments        {get; set;}
        data.core.GenericDalRepository<Composant>    Composants      {get; set;}
        data.core.GenericDalRepository<Composition>  Compositions    {get; set;}
        data.core.GenericDalRepository<Marque>       Marques         {get; set;}        

    }
}