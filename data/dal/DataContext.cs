using System.Collections.Generic;
using data.entities;

namespace data.dal
{
    public static class ExtensionMethods
    {
        public static IEnumerable<Composition> 
            GetByIdAliment(this data.core.GenericDalRepository<Composition> collection,long id)
            => collection.GetByCol("IdAliment",id);
    }

    public class DataContext : IDataContext
    {

        public DataContext()
        {
            Connection      = new MySql.Data.MySqlClient.MySqlConnection("server=gattaca;port=6033;database=Alimentation;user=root;password=MasterPassword");
            Aliments        = new AlimentDalRepository(Connection);
            Composants      = new data.core.GenericDalRepository<Composant>(Connection);
            Compositions    = new data.core.GenericDalRepository<Composition>(Connection) ;                                ;
            Marques         = new data.core.GenericDalRepository<Marque>(Connection);
            Recettes        = new data.core.GenericDalRepository<Recette>(Connection);
            Repas           = new data.core.GenericDalRepository<Repas>(Connection);
        }

        MySql.Data.MySqlClient.MySqlConnection              Connection      { get; set; }
        public AlimentDalRepository                         Aliments        { get; set; }
        public data.core.GenericDalRepository<Composant>    Composants      { get; set; }
        public data.core.GenericDalRepository<Composition>  Compositions    { get; set; }
        public data.core.GenericDalRepository<Marque>       Marques         { get; set; }
        public data.core.GenericDalRepository<Repas>        Repas           { get; set; }
        public data.core.GenericDalRepository<Recette>      Recettes        { get; set; }

    }
}