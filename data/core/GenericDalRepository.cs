//using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System;
using data.entities;
using Dapper;
using Dapper.Contrib.Extensions;
using MySql;

namespace data.core
{
    public class GenericDalRepository<T> where T : class
    {
        protected const string pSelectWhere = "SELECT * FROM {0} WHERE {1} = @Id";
        protected const string pDeleteWhere = "DELETE FROM {0} WHERE {1} = @Id";

        public GenericDalRepository() {}

        public GenericDalRepository(MySql.Data.MySqlClient.MySqlConnection connection) {    Connection = connection; }

        public MySql.Data.MySqlClient.MySqlConnection Connection { get; set; }

        public T GetById(long? id) => Connection.Get<T>(id);

        public long Insert(T entity) => Connection.Insert(entity);

        public bool Update(T entity) => Connection.Update(entity);

        public bool Delete(T entity) => Connection.Delete(entity);

        public bool Delete(long? id) 
        {
            if(id.HasValue)
            {
                var strColonne = (Attribute.GetCustomAttribute(typeof(T),typeof(TableAttribute)) as TableAttribute).Name;
                var strQuery = String.Format(pDeleteWhere,strColonne,"Id");
                var result = Connection.Execute ( strQuery, new { Id = id.Value });                
                return result>0;
            }
            return false;
            
        } 

        public System.Collections.Generic.IEnumerable<T> GetAll() => Connection.GetAll<T>();

        public System.Collections.Generic.IEnumerable<T> GetByCol(string colonneName,long id)
        {
            //var strColonne = typeof(T).GetCustomAttribute(typeof(TableAttribute)).Name.ToString();
            var strColonne = (Attribute.GetCustomAttribute(typeof(T),typeof(TableAttribute)) as TableAttribute).Name;
            var strQuery = String.Format(pSelectWhere,strColonne,colonneName);
            var query = Connection.QueryMultiple( strQuery, new { Id = id });
            return query.Read<T>();
        }
    }
}