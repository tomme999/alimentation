using Dapper.Contrib;
using Dapper.Contrib.Extensions;
using data.entities;

namespace data.core
{
    public class CoreEntity
    {
        [Key]
        public long Id 
        {
            get;
            set;
        }       
    }
}
