﻿using System.Data;
using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using data.entities;
using MySql;

namespace data
{
    public class Class1
    {
        public void init()
        {
            using(var connection = new MySql.Data.MySqlClient.MySqlConnection("server=gattaca;port=32768;database=Alimentation;user=root;password=password"))
            {
                    var aliments = connection.Query<data.entities.Aliment>("select * from Aliment");
                    foreach(data.entities.Aliment aliment in aliments)
                    {
                        System.Console.WriteLine(aliment.NomAliment);
                    }
                    
            }
        }
        public Aliment GetById(long id)
        {
            using(var connection = new MySql.Data.MySqlClient.MySqlConnection("server=gattaca;port=32768;database=Alimentation;user=root;password=password"))
            {
                var aliment = connection.QueryFirstOrDefault<data.entities.Aliment>("select * from Aliment where Id=@ParamId", new {ParamId = id});
                return aliment;
            }
        }
    }
}
