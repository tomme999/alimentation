import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlimentService {

  constructor( private http: HttpClient ) { }

  getAliments() {
    return this.http.get('localhost:5000/aliments/V2/aliments/');
  }
}
