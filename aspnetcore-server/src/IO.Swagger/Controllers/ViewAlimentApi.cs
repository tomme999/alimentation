/*
 * Swagger Petstore
 *
 * This is a sample server Petstore server.  You can find out more about     Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).      For this sample, you can use the api key `special-key` to test the authorization     filters.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apiteam@swagger.io
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using IO.Swagger.Attributes;
using IO.Swagger.Security;
using Microsoft.AspNetCore.Authorization;
using IO.Swagger.Models;
using IO.Swagger.ViewModelRepository;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class ViewAlimentApiController : ControllerBase
    { 
        public IViewAlimentRepository ViewAlimentRepository { get;    set; }

        public ViewAlimentApiController(IViewAlimentRepository aliment)
        {
            ViewAlimentRepository = aliment;
        }

        /// <summary>
        /// Find composant by ID
        /// </summary>
        /// <remarks>Returns a single composant</remarks>
        /// <param name="alimentId">ID of aliment to return</param>
        /// <response code="200">successful operation</response>
        /// <response code="400">Invalid ID supplied</response>
        /// <response code="404">composant not found</response>
        [HttpGet]
        [Route("/v2/views/aliment/{alimentId}")]
        //[Authorize(AuthenticationSchemes = ApiKeyAuthenticationHandler.SchemeName)]
        [ValidateModelState]
        [SwaggerOperation("GetViewAlimentById")]
        [SwaggerResponse(statusCode: 200, type: typeof(ViewAliment), description: "successful operation")]
        public virtual IActionResult GetViewAlimentById([FromRoute][Required]long? alimentId)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(ViewAliment));

            //TODO: Uncomment the next line to return response 400 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(400);

            //TODO: Uncomment the next line to return response 404 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(404);

            string exampleJson = null;
            exampleJson = "<null>\nnull\n</null>";
            exampleJson = "{\n  \"compositionAliment\" : \"\",\n  \"aliment\" : {\n    \"idMarque\" : 6,\n    \"nomAliment\" : \"nomAliment\",\n    \"id\" : 0\n  },\n  \"marque\" : {\n    \"id\" : 0,\n    \"nomMarque\" : \"nomMarque\"\n  }\n}";
            
            var example = exampleJson != null
            ? JsonConvert.DeserializeObject<ViewAliment>(exampleJson)
            : default(ViewAliment);
            example = ViewAlimentRepository.Get(alimentId);
            //TODO: Change the data returned
            return new ObjectResult(example);
        }
    }
}
