using Microsoft.AspNetCore.Mvc;
using IO.Swagger.ModelRepository;

namespace IO.Swagger.Helpers
{
    /// <summary>
    /// Classe d'extension pour les Controllers
    /// </summary>
    public static class ControlerExtension
    {
        public static IActionResult Add<T>(this ControllerBase controllerBase,GenericIModelRepository<T> modelRepository, T model) where T:class
        {
            var id = modelRepository.Insert(model);
            if  ( id>=0 )
            {
                return controllerBase.StatusCode(200);
            }
            else
            {
                return controllerBase.StatusCode(405);                
            }
        }

        public static IActionResult Delete<T>(this ControllerBase controllerBase,GenericIModelRepository<T> modelRepository, long? id) where T:class
        {
            if(id.HasValue && id.Value >= 0 )
            {
                if( modelRepository.Delete(id) )
                    return controllerBase.StatusCode(200);
                else
                    return controllerBase.StatusCode(404);
            }
            else
            {
                return controllerBase.StatusCode(400);
            }
        }

        public static IActionResult GetById<T>(this ControllerBase controllerBase,GenericIModelRepository<T> modelRepository, long? id) where T:class
        {
            if(id.HasValue)
            {
                if(id.Value>0)
                {
                    T model = modelRepository.GetById(id.Value);
                    if (model == null)
                    {
                        return controllerBase.NotFound();
                    }
                    else
                    {
                        return new ObjectResult(model);
                    }
                }
                else
                {
                    return controllerBase.BadRequest();
                }
            }
            else
            {
                return controllerBase.BadRequest();        
            }
            
        }
        
        /// <summary>
        /// Methode d'extension pour updater un model
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static IActionResult Update<T>(this ControllerBase controllerBase,GenericIModelRepository<T> modelRepository, T model) where T:class
        {
            var updated = modelRepository.Update(model);
            if  ( updated )
            {
                return controllerBase.StatusCode(200);
            }
            else
            {
                return controllerBase.StatusCode(405);                
            }
        }
    }
}