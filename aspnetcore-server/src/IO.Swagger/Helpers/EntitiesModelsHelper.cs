namespace IO.Swagger.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class EntitiesModelsHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="alimentEntity"></param>
        /// <param name="alimentModel"></param>
        public static void Copy(this data.entities.Aliment alimentEntity, Models.Aliment alimentModel)
        {  
            alimentEntity.Id          =   alimentModel.Id.Value;
            alimentEntity.NomAliment  =   alimentModel.NomAliment;
            alimentEntity.IdMarque    =   alimentModel.IdMarque;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="alimentModel"></param>
        /// <param name="alimentEntity"></param>
        public static void Copy(this Models.Aliment alimentModel, data.entities.Aliment alimentEntity)
        {  
            alimentModel.Id          =   alimentEntity.Id;
            alimentModel.NomAliment  =   alimentEntity.NomAliment;
            alimentModel.IdMarque    =   alimentEntity.IdMarque;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Models.Aliment GetById(long? id)
        {  
            if(id.HasValue)
            {
                var aliment = (new data.Class1()).GetById(id.Value);
                var alimentModel = new Models.Aliment();
                alimentModel.Copy(aliment);
                return alimentModel;
            }
            return null;
        }
    }
}