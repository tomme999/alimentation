/*
 * Swagger Petstore
 *
 * This is a sample server Petstore server.  You can find out more about     Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).      For this sample, you can use the api key `special-key` to test the authorization     filters.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: apiteam@swagger.io
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Aliment : IEquatable<Aliment>
    { 
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or Sets NomAliment
        /// </summary>
        [DataMember(Name="nomAliment")]
        public string NomAliment { get; set; }

        /// <summary>
        /// Gets or Sets IdMarque
        /// </summary>
        [DataMember(Name="idMarque")]
        public long? IdMarque { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Aliment {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  NomAliment: ").Append(NomAliment).Append("\n");
            sb.Append("  IdMarque: ").Append(IdMarque).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Aliment)obj);
        }

        /// <summary>
        /// Returns true if Aliment instances are equal
        /// </summary>
        /// <param name="other">Instance of Aliment to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Aliment other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    NomAliment == other.NomAliment ||
                    NomAliment != null &&
                    NomAliment.Equals(other.NomAliment)
                ) && 
                (
                    IdMarque == other.IdMarque ||
                    IdMarque != null &&
                    IdMarque.Equals(other.IdMarque)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (NomAliment != null)
                    hashCode = hashCode * 59 + NomAliment.GetHashCode();
                    if (IdMarque != null)
                    hashCode = hashCode * 59 + IdMarque.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Aliment left, Aliment right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Aliment left, Aliment right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
