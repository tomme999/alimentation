using IO.Swagger.Models;

namespace IO.Swagger.ViewModelRepository
{
    public interface IViewAlimentRepository
    {
         ViewAliment Get(long? id);
    }
}