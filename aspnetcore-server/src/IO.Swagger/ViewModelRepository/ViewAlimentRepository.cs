using System.Linq;
using IO.Swagger.ModelRepository;
using IO.Swagger.Models;

namespace IO.Swagger.ViewModelRepository
{
    public class ViewAlimentRepository:IViewAlimentRepository
    {
        IAlimentModelRepository AlimentModelRepository { get; set; }
        IComposantModelRepository ComposantModelRepository { get; set; }
        ICompositionModelRepository CompositionModelRepository { get; set; }
        public ViewAlimentRepository(IAlimentModelRepository aliment, IComposantModelRepository composant, ICompositionModelRepository composition)
        {
            AlimentModelRepository = aliment;
            ComposantModelRepository = composant;
            CompositionModelRepository = composition;
        }

        public ViewAliment Get(long? id)
        {
            if( !id.HasValue ) return null;

            var aliment = AlimentModelRepository.GetById(id);
            var composants = ComposantModelRepository.GetAll();
            var compositions = CompositionModelRepository.GetByIdAliment( id.Value );

            ViewAliment viewAliment = new ViewAliment();

            viewAliment.Aliment = aliment;
            viewAliment.CompositionAliment = new DefCompositionAliment();
            viewAliment.CompositionAliment.AddRange(
                from composant in composants
                    join composition in compositions on composant.Id equals composition.IdComposant
                    select new DefCompositionAlimentInner () 
                    { 
                        IdComposant     = composant.Id,
                        IdComposition   = composition.Id,
                        NomComposant    = composant.NomComposant,
                        Unite           = composant.Unite,
                        Valeur          = composition.Valeur
                    });
            
            return viewAliment;
        }
    }
}