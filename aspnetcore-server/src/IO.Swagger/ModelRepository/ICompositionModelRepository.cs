namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Interface de CompositionModelRepository necessaire a l'injection de dependences
    /// </summary>
    public interface ICompositionModelRepository:GenericIModelRepository<Models.Composition>
    {
         System.Collections.Generic.IEnumerable<Models.Composition> GetByIdAliment(long idAliment);
    }
}