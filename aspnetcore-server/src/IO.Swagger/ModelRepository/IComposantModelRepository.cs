namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Interface d'ComposantModelRepository necessaire a l'injection de dependences
    /// </summary>
    public interface IComposantModelRepository:GenericIModelRepository<Models.Composant>
    {
         
    }
}