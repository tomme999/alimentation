namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Interface d'MarqueModelRepository necessaire a l'injection de dependences
    /// </summary>
    public interface IMarqueModelRepository:GenericIModelRepository<Models.Marque>
    {
    }
}