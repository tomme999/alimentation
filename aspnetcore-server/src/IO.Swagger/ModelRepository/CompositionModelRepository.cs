using System.Linq;
using System.Collections.Concurrent;
using data.entities;
using IO.Swagger.Models;
using data.dal;

namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Manipulation des objets CompositionModel
    /// </summary>
    public class CompositionModelRepository: GenericModelRepository<Models.Composition,data.entities.Composition>,ICompositionModelRepository
    {
        #region Proprietes
        /// <summary>
        /// Datacontext pour la base de données
        /// </summary>
        /// <value></value>
        public data.dal.IDataContext DataContext {get; set;}
        #endregion
        #region Constructeurs
        /// <summary>
        /// Construteur pour injection de dependances
        /// </summary>
        /// <param name="dc">DataContext</param>
        public CompositionModelRepository(data.dal.IDataContext dc)
        {
            DataContext = dc;
        }
        #endregion
        #region Populates
        /// <summary>
        /// Conversion d'une entite en model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="entity"></param>
        protected override void populate(Models.Composition model, data.entities.Composition entity)
        {
            model.Id            =   entity.Id;
            model.IdAliment     =   entity.IdAliment;
            model.IdComposant   =   entity.IdComposant;
            model.Valeur        =   entity.Valeur;
        }
        /// <summary>
        /// Conversion d'un model en une entite
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        protected override void populate(data.entities.Composition entity, Models.Composition model)
        {
            entity.Id            =   model.Id.Value;
            entity.IdAliment     =   model.IdAliment.Value;
            entity.IdComposant   =   model.IdComposant.Value;
            model.Valeur         =   entity.Valeur;
        }
        #endregion
       #region Implementation interface ICompositionModelRepository
        /// <summary>
        /// Retourne un model Composition par id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Models.Composition GetById(long? id) => populate(DataContext.Compositions.GetById(id));

        /// <summary>
        /// Mise à jour d'un model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(Models.Composition model) => DataContext.Compositions.Update(populate(model));

        /// <summary>
        /// Insertion d'un model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public long Insert(Models.Composition model) 
        { 
            model.Id = DataContext.Compositions.Insert( populate( model) );
            return model.Id.Value;
        }

        /// <summary>
        /// Suppression d'un model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Delete(Models.Composition model) => DataContext.Compositions.Delete( populate( model) );

        public bool Delete(long ? id) => DataContext.Compositions.Delete( id );
        public System.Collections.Generic.IEnumerable<Models.Composition> GetAll()
            => DataContext.Compositions.GetAll().Select( c => populate(c) );
        public System.Collections.Generic.IEnumerable<Models.Composition> GetByIdAliment(long idAliment)
            => DataContext.Compositions.GetByIdAliment(idAliment).Select( c => populate(c) );

        #endregion      
    }
}