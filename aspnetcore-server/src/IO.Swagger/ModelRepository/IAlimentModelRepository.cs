using data.entities;
using IO.Swagger.Models;

namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Interface d'AlimentModelRepository necessaire a l'injection de dependences
    /// </summary>
    public interface IAlimentModelRepository:GenericIModelRepository<Models.Aliment>
    {   
    }
}