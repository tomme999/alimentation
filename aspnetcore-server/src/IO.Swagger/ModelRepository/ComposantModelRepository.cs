using System.Linq;
using System.Collections.Concurrent;
using data.entities;
using IO.Swagger.Models;


namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Manipulation des objets ComposantModel
    /// </summary>
    public class ComposantModelRepository: GenericModelRepository<Models.Composant,data.entities.Composant>,IComposantModelRepository
    {
        #region Proprietes
        /// <summary>
        /// Datacontext pour la base de données
        /// </summary>
        /// <value></value>
        public data.dal.IDataContext DataContext {get; set;}
        #endregion
        #region Constructeurs
        /// <summary>
        /// Construteur pour injection de dependances
        /// </summary>
        /// <param name="dc">DataContext</param>
        public ComposantModelRepository(data.dal.IDataContext dc)
        {
            DataContext = dc;
        }
        #endregion
        #region Populates
        /// <summary>
        /// Conversion d'une entite en model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="entity"></param>
        protected override void populate(Models.Composant model, data.entities.Composant entity)
        {
            model.Id            =   entity.Id;
            model.NomComposant  =   entity.NomComposant;
            model.Unite         =   entity.Unite;
        }
        /// <summary>
        /// Conversion d'un model en une entite
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        protected override void populate(data.entities.Composant entity, Models.Composant model)
        {
            entity.Id               =   model.Id.Value;
            entity.NomComposant     =   model.NomComposant;
            entity.Unite            =   model.Unite;
        }
        #endregion
        #region Implementation interface IComposantModelRepository
        /// <summary>
        /// Retourne un model Composant par id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Models.Composant GetById(long? id) => populate(DataContext.Composants.GetById(id));

        /// <summary>
        /// Mise à jour d'un model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(Models.Composant model) => DataContext.Composants.Update(populate(model));

        /// <summary>
        /// Insertion d'un model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public long Insert(Models.Composant model) 
        { 
            model.Id = DataContext.Composants.Insert( populate( model) );
            return model.Id.Value;
        }

        /// <summary>
        /// Suppression d'un model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Delete(Models.Composant model) => DataContext.Composants.Delete( populate( model) );

        public bool Delete(long ? id) => DataContext.Composants.Delete( id );

        public System.Collections.Generic.IEnumerable<Models.Composant> GetAll()
            => DataContext.Composants.GetAll().Select( c => populate(c) );

        #endregion
    }
}