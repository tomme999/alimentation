using System.Collections.Concurrent;
using System.Linq;
using data.entities;
using IO.Swagger.Models;

namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Manipulation des objets AlimentModel
    /// </summary>
    public class AlimentModelRepository: GenericModelRepository<Models.Aliment,data.entities.Aliment>,IAlimentModelRepository
    {
        /// <summary>
        /// Datacontext pour la base de données
        /// </summary>
        /// <value></value>
        public data.dal.IDataContext DataContext {get; set;}
        
        /// <summary>
        /// Construteur pour injection de dependances
        /// </summary>
        /// <param name="dc">DataContext</param>
        public AlimentModelRepository(data.dal.IDataContext dc)
        {
            DataContext = dc;
        }
        /// <summary>
        /// Conversion d'une entite en model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="entity"></param>
        protected override void populate(Models.Aliment model, data.entities.Aliment entity)
        {
            if( model != null && entity != null )
            {
                model.Id           =   entity.Id;
                model.IdMarque     =   entity.IdMarque;
                model.NomAliment   =   entity.NomAliment;
            }
        }
        /// <summary>
        /// Conversion d'un model en une entite
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        protected override void populate(data.entities.Aliment entity, Models.Aliment model)
        {
            if( model != null && entity != null )
            {
                entity.Id           =   model.Id.Value;
                entity.IdMarque     =   model.IdMarque;
                entity.NomAliment   =   model.NomAliment;
            }
        }

        public Models.Aliment GetById(long? id) => populate(DataContext.Aliments.GetById(id));

        public bool Update(Models.Aliment model) => DataContext.Aliments.Update(populate(model));

        public long Insert(Models.Aliment model) => DataContext.Aliments.Insert( populate( model) );

        public bool Delete(Models.Aliment model) => DataContext.Aliments.Delete( populate( model) );

        public bool Delete( long? id) => DataContext.Aliments.Delete( id );

        public System.Collections.Generic.IEnumerable<Models.Aliment> GetAll()
            => DataContext.Aliments.GetAll().Select( c => populate(c) );
    }
}