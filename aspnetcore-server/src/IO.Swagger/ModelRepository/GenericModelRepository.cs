

namespace IO.Swagger.ModelRepository
{
    public class GenericModelRepository<MODEL,ENTITY> where MODEL:class,new() where ENTITY:class,new()
    {
        protected virtual void populate(MODEL model, ENTITY entity) {}
        protected virtual void populate(ENTITY entity, MODEL model) {}
        protected MODEL populate(ENTITY entity)
        {
            MODEL model = new MODEL();
            populate(   model,  entity  );
            return model;
        }
        protected ENTITY populate(MODEL model)
        {
            ENTITY entity = new ENTITY();
            populate(   entity, model   );
            return entity;
        }
    }
}