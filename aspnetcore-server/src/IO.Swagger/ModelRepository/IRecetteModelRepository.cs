namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Interface d'RecetteModelRepository necessaire a l'injection de dependences
    /// </summary>
    public interface IRecetteModelRepository:GenericIModelRepository<Models.Recette>
    {
         
    }
}