using System.Collections.Concurrent;
using System.Linq;
using data.entities;
using IO.Swagger.Models;

namespace IO.Swagger.ModelRepository
{
    /// <summary>
    /// Manipulation des objets MarqueModel
    /// </summary>
    public class MarqueModelRepository: GenericModelRepository<Models.Marque,data.entities.Marque>,IMarqueModelRepository
    {
        /// <summary>
        /// Datacontext pour la base de données
        /// </summary>
        /// <value></value>
        public data.dal.IDataContext DataContext {get; set;}

        public MarqueModelRepository(data.dal.IDataContext dc)
        {
            DataContext = dc;
        }


        protected override void populate(Models.Marque model, data.entities.Marque entity)
        {
            model.Id           =   entity.Id;
            model.NomMarque    =   entity.NomMarque;
        }

        protected override void populate(data.entities.Marque entity, Models.Marque model)
        {
            entity.Id           =   model.Id.Value;
            entity.NomMarque    =   model.NomMarque;
        }

        public Models.Marque GetById(long? id) => populate(DataContext.Marques.GetById(id));

        public bool Update(Models.Marque model) => DataContext.Marques.Update( populate(model) );

        public long Insert(Models.Marque model) => DataContext.Marques.Insert( populate( model) );

        public bool Delete(Models.Marque model) => DataContext.Marques.Delete( populate( model) );

        public bool Delete(long ? id) => DataContext.Marques.Delete( id );

        public System.Collections.Generic.IEnumerable<Models.Marque> GetAll()
            => DataContext.Marques.GetAll().Select( c => populate(c) );
    }
}