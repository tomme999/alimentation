using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices.ComTypes;
namespace IO.Swagger.ModelRepository
{
    public interface GenericIModelRepository<T> where T:class
    {
         T GetById(long? id);

        bool Update(T model) ;    

        long Insert(T model) ;    

        bool Delete(T model) ;  

        bool Delete(long ? id);

        System.Collections.Generic.IEnumerable<T> GetAll();
    }
}